//              VARIATION 1            

function login(name, age) {
    name = prompt("Введіть ім'я", name);
    age = +prompt("Скільки вам років", age);
    
    if (!name || !age || isNaN(age)) {
        login(name, age)
        return
    }

    if (age < 18) {
        alert("You are not allowed to visit this website");
    } else if (age > 22) {
        alert("Welcome, " + name);
    } else {
        const confirmed = confirm("Are you sure you want to continue?");

        if (confirmed) {
            alert("Welcome, " + name);
        } else {
            alert("You are not allowed to visit this website");
        }
    }
}
login()

//              VARIATION 2

// let name, age;

// while (!name || !age || isNaN(age)) {
//     name = prompt("Введіть ім'я", name);
//     age = +prompt("Скільки вам років", age);
// }

// if (age < 18) {
//     alert("You are not allowed to visit this website");
// } else if (age > 22) {
//     alert("Welcome, " + name);
// } else {
//     const confirmed = confirm("Are you sure you want to continue?");

//     if (confirmed) {
//         alert("Welcome, " + name);
//     } else {
//         alert("You are not allowed to visit this website");
//     }
// }